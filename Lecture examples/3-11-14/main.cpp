/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 11, 2014, 11:13 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int num;
    //always ask user for input when needed
    cout << "Enter an integer: " << endl;
    cin >> num;
    if (num > 0)
    {
        cout << "you entered a positive number" << endl;
    }
    else if (num<0)
    {
        cout << "you entered a negative number"<< endl;
    }
    else
    {
        cout << "You entered the number 0"<< endl;
    }
    int switchNum;
    cout << "Enter a number between 1-3"<< endl;
    cin >>switchNum;
    
    switch (switchNum)
    {
        case 1:
        cout <<"You entered the Number 1"<< endl;
        break;
        case 2:
        cout <<"You entered the Number 2"<< endl;
        break;
        case 3:
        cout <<"You entered the Number 3"<< endl;
        break;
        default:
        cout <<"You did not enter a valid Number"<<endl;
        break;
    }
    int x = 0;
    while (x< 1000000000)
    {
        cout << x << endl;
        x++;
    }
    return 0;
}

