/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 27, 2014, 10:52 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    const double randNum = 12.3259;
    //randNum = 12.5;
    cout << setprecision(2);
    cout << randNum<< endl;
    cout <<fixed<< randNum<<endl;
    cout <<setw(10) <<randNum<<randNum;
    
    cout <<setw(10)<<left<<randNum<<randNum<<endl;
  
    
    return 0;
}

