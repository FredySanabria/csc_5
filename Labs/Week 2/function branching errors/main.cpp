/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 4, 2014, 11:39 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string message = "Hello World";
    
    cout << message.substr(2.4) << endl;
    
    cout << message.substr(7) << endl;
    
    cout << message.find("llo")<< endl;
    
    cout <<message.length()<<endl;
    cout <<message.size()<<endl;
    
    int x=3;
    if (x<5)
    {    
     x = 5;   
    }
    cout <<"x: "<<x<<endl;
    if (x!=5)
    {
        x++;
    }
    else
    {
        x--;
    }
    cout <<"x: "<<x <<endl;
    return 0;
}

