/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 13, 2014, 11:55 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "Please enter your grade percentage"<< endl;
    int usergrade;
    cin >> usergrade;
    if (usergrade > 89)
    {
        cout << "You have an A"<< endl;
    }
    else if (usergrade > 79)
    {
        cout << "You have a B" << endl;
    }
    else if (usergrade > 69)
    {
        cout << "You have a C" << endl;
    }
    else if (usergrade > 59)
    {
        cout << "You have a D" << endl;
    }
    else 
    {
        cout << "You have an F" << endl;
    }
    return 0;
}

