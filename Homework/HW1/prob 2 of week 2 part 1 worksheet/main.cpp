/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 27, 2014, 11:33 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int a = 30;
    int b = 10;
    int temp = a;
    
    cout << " a: " << a << " b: " << b << endl;
    a = b;
    b = temp;
    cout << " a: " << a << " b: " << b << endl;
    return 0;
}

