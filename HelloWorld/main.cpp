/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 20, 2014, 10:30 AM
 */

//#include <cstdlib>// not needed for output
#include <iostream>// iostream is need for output
// a slite change to this file is needed
//gives the context of where the libraries are coming from
using namespace std;

/*
 * 
 */
//there is always and only one main
//programs execute to top to bottom, left to right
int main(int argc, char** argv) {
    //cout speciefys output 
    //endl creates a new line
    //<< speciefies stream operater
    //all staments end in semiicolon
    //im going to use a programer defined variable
    // test for git pull
    string message ;
    message = "Hello World";        
    cout<< message << endl;
    
    cout<< "Fredy Sanabria";
    //if program gets to return it ran successfully
    
    return 0;
}

